package com.testproject.di

import android.content.Context
import android.content.res.Resources
import com.testproject.data.DataManager
import com.testproject.data.repository.TestApiRepository
import com.testproject.rest.TestAppService
import dagger.Module
import dagger.Provides

/**
@author Viktor Vaidner v.vaidner@theappsolutions.com
@copyright (c) 2017 TheAppSolutions. (https://theappsolutions.com)
@project testproject
 */

@Module
class ApplicationModule (private val context: Context) {

    @Provides
    fun provideContext(): Context = context

    @Provides
    fun provideApiManager(testAppService: TestAppService): TestApiRepository {
        return TestApiRepository(testAppService)
    }

    @Provides
    fun provideInstanceInteractor(resources: Resources,
                                           testApiRepository: TestApiRepository): DataManager {
        return DataManager(resources, testApiRepository)
    }

    @Provides
    fun provideResources(): Resources {
        return context.resources
    }
}
