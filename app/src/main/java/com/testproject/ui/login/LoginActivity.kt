package com.testproject.ui.login

import android.content.Intent
import android.os.Bundle
import com.testproject.R
import com.testproject.TestApp
import com.testproject.common.mvp.MvpActivity
import com.testproject.ui.main.MainActivity
import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject

class LoginActivity : MvpActivity<LoginActivityView, LoginActivityPresenter>(), LoginActivityView {

    @Inject
    lateinit var presenterLogin: LoginActivityPresenterImpl

    override fun onCreate(savedInstanceState: Bundle?) {
        TestApp[this].component.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        button_ok.setOnClickListener({
            val login = edit_text_login.text.toString()
            val password = edit_text_password.text.toString()
            presenter?.login(login, password)
        })
    }

    override val layout: Int
        get() = R.layout.activity_login

    override fun createPresenter(): LoginActivityPresenter {
        return presenterLogin
    }

    override fun provideLogin() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    override fun showErrorMessage(errorMessage: String) {
        showDialogErrorMessage(errorMessage);
    }

    override fun removeWait() {
        removeProgressWait()
    }

    override fun showWait() {
        showProgressWait()
    }
}
