package com.testproject.data.repository

import com.testproject.model.WorldsData
import com.testproject.rest.TestAppService
import com.testproject.utils.DEVICE_TYPE
import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers

/**
@author Viktor Vaidner v.vaidner@theappsolutions.com
@copyright (c) 2018 TheAppSolutions. (https://theappsolutions.com)
@project testproject
 */
class TestApiRepository(private val testAppService: TestAppService) {

    fun getWorlds(login: String, password: String, deviceId: String): Flowable<WorldsData> {
        return testAppService.getWorlds(login, password, DEVICE_TYPE, deviceId)
                .subscribeOn(Schedulers.io())
    }
}
