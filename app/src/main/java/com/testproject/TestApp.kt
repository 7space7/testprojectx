package com.testproject

import android.app.Activity
import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import com.facebook.stetho.Stetho
import com.testproject.di.ApplicationComponent
import com.testproject.di.ApplicationModule
import com.testproject.di.DaggerApplicationComponent
import timber.log.Timber

/**
@author Viktor Vaidner v.vaidner@theappsolutions.com
@copyright (c) 2018 TheAppSolutions. (https://theappsolutions.com)
@project testproject
 */
class TestApp : Application() {
    private lateinit var applicationComponent: ApplicationComponent

    init {
        instance = this
    }

    val component: ApplicationComponent
        get() = applicationComponent


    override fun onCreate() {
        super.onCreate()
        instance = this
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
            Stetho.initializeWithDefaults(this)
        }
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(applicationContext))
                .build()
    }

    companion object {
        lateinit var instance: TestApp

        operator fun get(activity: Activity): TestApp {
            return activity.application as? TestApp ?: throw IllegalStateException()
        }

        fun applicationContext(): Context? {
            return instance.applicationContext
        }

        fun hasNetwork(): Boolean {
            return instance.checkIfHasNetwork()
        }
    }

    fun checkIfHasNetwork(): Boolean {
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = cm.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }
}