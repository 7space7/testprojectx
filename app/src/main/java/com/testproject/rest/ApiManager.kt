package com.testproject.rest

import android.content.Context
import android.content.res.Resources
import com.google.gson.Gson
import com.readystatesoftware.chuck.ChuckInterceptor
import com.testproject.TestApp
import com.testproject.utils.CONNECTION_TIMEOUT
import com.testproject.utils.Utils.noConnectionException
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
@author Viktor Vaidner v.vaidner@theappsolutions.com
@copyright (c) 2018 TheAppSolutions. (https://theappsolutions.com)
@project testproject
 */
class ApiManager @Inject constructor(apiBaseUrl: String, gson: Gson,
                                     baseContext: Context,
                                     resources: Resources) {

    private val okHttpClient: OkHttpClient
    val retrofit: Retrofit


    init {
        okHttpClient = OkHttpClient.Builder()
                .connectTimeout(HTTP_TIMEOUT, TimeUnit.MILLISECONDS)
                .writeTimeout(HTTP_TIMEOUT, TimeUnit.MILLISECONDS)
                .readTimeout(HTTP_TIMEOUT, TimeUnit.MILLISECONDS)
                .addInterceptor(ChuckInterceptor(baseContext))
                .addInterceptor(ConnectionCheckInterceptor(baseContext))
                .build()

        retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(apiBaseUrl)
                .client(okHttpClient)
                .build()
    }

    private class ConnectionCheckInterceptor(private val appContext: Context) : Interceptor {

        @Throws(IOException::class)
        override fun intercept(chain: Interceptor.Chain): Response {
            if (!TestApp.hasNetwork()) {
                throw noConnectionException()
            }
            return chain.proceed(chain.request())
        }
    }

    companion object {
        private val HTTP_TIMEOUT = CONNECTION_TIMEOUT
    }
}
