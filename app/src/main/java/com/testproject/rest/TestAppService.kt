package com.testproject.rest

import com.testproject.model.WorldsData
import io.reactivex.Flowable
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Query

/**
@author Viktor Vaidner v.vaidner@theappsolutions.com
@copyright (c) 2018 TheAppSolutions. (https://theappsolutions.com)
@project testproject
 */
interface TestAppService {

    @Headers("Accept: application/json")
    @POST("worlds")
    fun getWorlds(@Query("login") login: String,
                  @Query("password") password: String,
                  @Query("deviceType") deviceType: String,
                  @Query("deviceId") deviceId: String): Flowable<WorldsData>
}