package com.testproject.di

import com.testproject.ui.login.LoginActivity
import com.testproject.ui.main.MainActivity
import dagger.Component

/**
@author Viktor Vaidner v.vaidner@theappsolutions.com
@copyright (c) 2018 TheAppSolutions. (https://theappsolutions.com)
@project testproject
 */
@Component(modules = [ApplicationModule::class,ApiModule::class])
interface ApplicationComponent {

    fun inject(mainActivity: MainActivity)
    fun inject(loginActivity: LoginActivity)

}
