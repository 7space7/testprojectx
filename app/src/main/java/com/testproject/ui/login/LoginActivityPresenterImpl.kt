package com.testproject.ui.login

import com.testproject.common.mvp.MvpPresenterImpl
import com.testproject.data.DataManager
import com.testproject.utils.API_LOGIN
import com.testproject.utils.API_PASSWORD
import javax.inject.Inject

class LoginActivityPresenterImpl @Inject constructor(private val dataManager: DataManager)
    : MvpPresenterImpl<LoginActivityView>(), LoginActivityPresenter {

    override fun login(login: String, password: String) {
        getView()?.showWait()
        if (login.equals(API_LOGIN) && password.equals(API_PASSWORD)) {
            getView()?.removeWait()
            getView()?.provideLogin()
        } else {
            getView()?.removeWait()
            val errorMessage = "Login or Password not correct"
            getView()?.showErrorMessage(errorMessage)
        }
    }
}