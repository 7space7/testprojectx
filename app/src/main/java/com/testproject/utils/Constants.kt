package com.testproject.utils

import java.util.concurrent.TimeUnit


/**
@author Viktor Vaidner v.vaidner@theappsolutions.com
@copyright (c) 2018 TheAppSolutions. (https://theappsolutions.com)
@project testproject
 */

const val API_BASE_URL = "http://backend1.lordsandknights.com/XYRALITY/WebObjects/BKLoginServer.woa/wa/"

const val API_LOGIN = "android.test@xyrality.com"

const val API_PASSWORD = "password"

val DEVICE_TYPE= String.format("%s %s", android.os.Build.MODEL, android.os.Build.VERSION.RELEASE)

val CONNECTION_TIMEOUT = TimeUnit.SECONDS.toMillis(10)

const val EXTRA_INFORM_DIALOG_TITLE = "extra_inform_dialog_title"

const val EXTRA_WORLDS_DATA = "extra_worlds_data"


