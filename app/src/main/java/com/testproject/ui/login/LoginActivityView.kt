package com.testproject.ui.login

import com.testproject.common.mvp.MvpView

interface LoginActivityView : MvpView {
    fun provideLogin()
    fun showErrorMessage(errorMessage: String)
    fun removeWait()
    fun showWait()
}