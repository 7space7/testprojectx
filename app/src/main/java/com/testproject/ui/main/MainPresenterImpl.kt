package com.testproject.ui.main

import com.testproject.common.mvp.MvpPresenterImpl
import com.testproject.data.DataManager
import com.testproject.utils.API_LOGIN
import com.testproject.utils.API_PASSWORD
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber
import javax.inject.Inject


/**
@author Viktor Vaidner v.vaidner@theappsolutions.com
@copyright (c) 2018 TheAppSolutions. (https://theappsolutions.com)
@project testproject
 */
class MainPresenterImpl @Inject constructor(private val dataManager: DataManager)
    : MvpPresenterImpl<MainActivityView>(), MainActivityPresenter {

    private val disposables = CompositeDisposable()

    override fun provideWorlds(deviceId: String) {
        getView()?.showWait()
        val disposable = dataManager.getWorlds(API_LOGIN, API_PASSWORD, deviceId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ worlds ->
                    getView()?.let {
                        it.removeWait()
                        it.showWorldsList(worlds.allAvailableWorlds)
                    }
                }, { throwable ->
                    // onError
                    Timber.v(throwable.toString())
                    getView()?.removeWait()
                })
        disposables.add(disposable)
    }

    override fun detachView() {
        disposables.let {
            if (it.isDisposed) {
                it.clear()
            }
        }
        super.detachView()
    }
}