package com.testproject.ui.login

import com.testproject.common.mvp.MvpPresenter

interface LoginActivityPresenter : MvpPresenter<LoginActivityView> {
    fun login(login: String, password: String)
}