package com.testproject

import android.support.v7.app.AppCompatActivity

/**
@author Viktor Vaidner v.vaidner@theappsolutions.com
@copyright (c) 2018 TheAppSolutions. (https://theappsolutions.com)
@project testproject
 */
open class SuperActivity:AppCompatActivity() {}