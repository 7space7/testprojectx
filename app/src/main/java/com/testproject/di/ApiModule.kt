package com.testproject.di

import android.accounts.AccountManager
import android.app.Application
import android.content.Context
import android.content.res.Resources
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.testproject.rest.ApiManager
import com.testproject.rest.TestAppService
import com.testproject.utils.API_BASE_URL
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
@author Viktor Vaidner v.vaidner@theappsolutions.com
@copyright (c) 2018 TheAppSolutions. (https://theappsolutions.com)
@project testproject
 */
@Module
class ApiModule {
    @Provides
    fun provideGson(): Gson {
        return GsonBuilder()
                .create()
    }

    @Provides
    fun provideApiManager(context: Context, gson: Gson, resources: Resources): ApiManager {
        return ApiManager(API_BASE_URL, gson, context, resources)
    }

    @Provides
    fun provideApi(apiManager: ApiManager): TestAppService {
        return apiManager.retrofit.create(TestAppService::class.java)
    }
}
