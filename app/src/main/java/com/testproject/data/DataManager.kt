package com.testproject.data

import android.content.res.Resources
import com.testproject.data.repository.TestApiRepository
import com.testproject.model.WorldsData
import io.reactivex.Flowable

/**
@author Viktor Vaidner v.vaidner@theappsolutions.com
@copyright (c) 2018 TheAppSolutions. (https://theappsolutions.com)
@project testproject
 */
class DataManager(private val resources: Resources,
                  private val testApiRepository: TestApiRepository) {


    fun getWorlds(login: String, password: String, deviceId: String): Flowable<WorldsData> {
        return testApiRepository.getWorlds(login, password, deviceId)
    }
}
