package com.testproject.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Worlds(var id: Int,
                  var country: String = "",
                  var language: String = "",
                  var mapURL: String = "",
                  var url: String = "",
                  var name: String = "",
                  var worldStatus:WorldStatus) : Parcelable