package com.testproject.ui.main

import com.testproject.common.mvp.MvpPresenter

/**
@author Viktor Vaidner v.vaidner@theappsolutions.com
@copyright (c) 2018 TheAppSolutions. (https://theappsolutions.com)
@project testproject
 */
interface MainActivityPresenter : MvpPresenter<MainActivityView> {
    fun provideWorlds(deviceId: String)
}