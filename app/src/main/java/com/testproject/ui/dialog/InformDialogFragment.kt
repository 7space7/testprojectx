package com.testproject.ui.dialog

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import android.widget.TextView
import com.testproject.R
import com.testproject.utils.EXTRA_INFORM_DIALOG_TITLE

/**
@author Viktor Vaidner v.vaidner@theappsolutions.com
@copyright (c) 2017 TheAppSolutions. (https://theappsolutions.com)
@project testproject
 */

class InformDialogFragment : DialogFragment() {

    private var title: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            title = arguments?.getString(EXTRA_INFORM_DIALOG_TITLE, "")
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity!!, R.style.CustomDialog)
        val inflater = activity!!.layoutInflater
        val view = inflater.inflate(R.layout.dialog_fragment_inform, null)
        val text_title = view.findViewById(R.id.text_title) as TextView
        text_title.text = title

        builder.setView(view)
        return builder.create()
    }

    companion object {
        fun newInstance(title: String): InformDialogFragment {
            val fragment = InformDialogFragment()
            val args = Bundle()
            args.putString(EXTRA_INFORM_DIALOG_TITLE, title)
            fragment.arguments = args
            return fragment
        }
    }
}
