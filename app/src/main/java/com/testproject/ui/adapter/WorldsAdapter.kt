package com.testproject.ui.adapter

import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.testproject.R
import com.testproject.model.Worlds

/**
@author Viktor Vaidner v.vaidner@theappsolutions.com
@copyright (c) 2018 TheAppSolutions. (https://theappsolutions.com)
@project testproject
 */

class WorldsAdapter : RecyclerView.Adapter<WorldsAdapter.Holder>() {

    private val _data: MutableList<Worlds> = mutableListOf()
    private var _callback: (Worlds) -> Unit = { }

    var data: List<Worlds>
        get() = _data
        set(value) {
            _data.clear()
            _data.addAll(value)
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_worlds, parent, false)
        return Holder(itemView)
    }

    override fun getItemCount() = _data.size

    override fun onBindViewHolder(holder: Holder, position: Int): Unit = with(_data[position]) {
        holder.apply {
            nameText.text = name
            statusText.text = worldStatus.description
        }
    }

    fun setCallback(callback: (Worlds) -> Unit) {
        _callback = callback
    }

    class Holder(view: View) : RecyclerView.ViewHolder(view) {
        val nameText: TextView = itemView.findViewById(R.id.text_name)
        val item_worlds: ConstraintLayout = itemView.findViewById(R.id.item_worlds)
        val statusText: TextView = itemView.findViewById(R.id.text_status)
    }
}