package com.testproject.common.mvp

import com.testproject.R
import io.reactivex.annotations.NonNull
import io.reactivex.functions.Consumer
import timber.log.Timber
import java.lang.ref.SoftReference
import java.util.*
import java.util.function.Supplier

/**
@author Viktor Vaidner v.vaidner@theappsolutions.com
@copyright (c) 2018 TheAppSolutions. (https://theappsolutions.com)
@project testproject
 */
open class MvpPresenterImpl<V : MvpView> : MvpPresenter<V> {

    private var viewReference: SoftReference<V>? = null

    fun getView(): V? = if (viewReference == null) null else viewReference?.get()

    override fun isViewAttached(): Boolean = viewReference != null && viewReference?.get() != null


    override fun attachView(view: V) {
        viewReference = SoftReference(view)
        onViewAttached()
    }

    override fun detachView() {
        if (viewReference != null) {
            viewReference?.clear()
            viewReference = null
        }
    }

    fun onViewAttached() {
    }
}