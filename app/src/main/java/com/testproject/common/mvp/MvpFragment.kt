package com.testproject.common.mvp

import android.app.ProgressDialog
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.testproject.R
import com.testproject.ui.dialog.InformDialogFragment

/**
@author Viktor Vaidner v.vaidner@theappsolutions.com
@copyright (c) 2018 TheAppSolutions. (https://theappsolutions.com)
@project testproject
 */
abstract class MvpFragment<V : MvpView, P : MvpPresenter<V>> : Fragment(), MvpView {
    protected var presenter: P? = null
    private var progressDialog: ProgressDialog? = null

    @get:LayoutRes
    abstract val layout: Int

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(layout, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (presenter == null) {
            presenter = createPresenter()
        }

        presenter?.attachView(this as V)

    }

    protected abstract fun createPresenter(): P

    abstract fun onBackPressed()


    override fun onDestroy() {
        presenter?.detachView()
        removeProgressWait()
        super.onDestroy()
    }

    override fun showDialogErrorMessage(message: String) {
        val informDialogFragment = InformDialogFragment.newInstance(message)
        informDialogFragment.show(childFragmentManager, InformDialogFragment::class.simpleName)
    }

    override fun showProgressWait() {
        progressDialog = ProgressDialog(context, R.style.ProgressDialogStyle)
        progressDialog?.show()
    }

    override fun removeProgressWait() {
        progressDialog?.let {
            if(it.isShowing){
                it.dismiss()
            }
        }
    }

    override fun showToastMessage(text: String) {
        Toast.makeText(context, text, Toast.LENGTH_LONG).show()
    }

    override fun showToastMessage(textRes: Int) {
        Toast.makeText(context, getString(textRes), Toast.LENGTH_LONG).show()
    }
}
