package com.testproject.common.mvp

/**
@author Viktor Vaidner v.vaidner@theappsolutions.com
@copyright (c) 2018 TheAppSolutions. (https://theappsolutions.com)
@project testproject
 */
interface MvpView {
    fun showProgressWait()

    fun removeProgressWait()

    fun showToastMessage(text: String)

    fun showToastMessage(textRes: Int)

    fun showDialogErrorMessage(errorMessage: String)
}