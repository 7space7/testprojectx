package com.testproject.common.mvp

import android.app.ProgressDialog
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.widget.Toast
import com.testproject.R
import com.testproject.SuperActivity
import com.testproject.ui.dialog.InformDialogFragment

/**
@author Viktor Vaidner v.vaidner@theappsolutions.com
@copyright (c) 2018 TheAppSolutions. (https://theappsolutions.com)
@project testproject
 */
abstract class MvpActivity<V : MvpView, P : MvpPresenter<V>> : SuperActivity(), MvpView {
    protected var presenter: P? = null
    private var progressDialog: ProgressDialog? = null

    @get:LayoutRes
    protected abstract val layout: Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (layout != 0) {
            setContentView(layout)
        }
        if (presenter == null) {
            presenter = createPresenter()
        }
        presenter?.attachView(this as V)

    }

    override fun onDestroy() {
        removeProgressWait()
        presenter?.detachView()
        super.onDestroy()
    }


    protected abstract fun createPresenter(): P

    override fun showDialogErrorMessage(errorMessage: String) {
        val informDialogFragment = InformDialogFragment.newInstance(errorMessage)
        informDialogFragment.show(supportFragmentManager, InformDialogFragment::javaClass.name)
    }

    override fun showProgressWait() {
        progressDialog = ProgressDialog(this, R.style.ProgressDialogStyle)
        progressDialog?.show()
    }

    override fun removeProgressWait() {
        progressDialog?.let {
            if (it.isShowing) {
                it.dismiss()
            }
        }
    }

    override fun showToastMessage(text: String) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show()
    }

    override fun showToastMessage(textRes: Int) {
        Toast.makeText(this, getString(textRes), Toast.LENGTH_LONG).show()
    }

    companion object {
        private val TAG = MvpActivity::class.java.simpleName
    }
}
