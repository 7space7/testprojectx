package com.testproject.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class WorldStatus(var id: Int,
                       var description: String = ""): Parcelable