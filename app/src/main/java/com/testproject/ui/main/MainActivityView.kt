package com.testproject.ui.main

import com.testproject.common.mvp.MvpView
import com.testproject.model.Worlds

/**
@author Viktor Vaidner v.vaidner@theappsolutions.com
@copyright (c) 2018 TheAppSolutions. (https://theappsolutions.com)
@project testproject
 */
interface MainActivityView : MvpView {
    fun showWorldsList(worlds: ArrayList<Worlds>?)
    fun showErrorMessage(errorMessage: String)
    fun removeWait()
    fun showWait()
}