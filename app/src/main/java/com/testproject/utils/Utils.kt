package com.testproject.utils


import com.testproject.R
import com.testproject.TestApp
import com.testproject.common.misc.NoConnectionException


object Utils {

    fun noConnectionException(text: String): NoConnectionException {
        return NoConnectionException(TestApp.applicationContext()?.getResources()
                ?.getString(R.string.no_internet_connection) + " " + text)
    }

    fun noConnectionException(): NoConnectionException {
        return noConnectionException("")
    }
}