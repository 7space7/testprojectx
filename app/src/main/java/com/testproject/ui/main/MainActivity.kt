package com.testproject.ui.main

import android.os.Bundle
import android.provider.Settings.Secure
import android.support.v7.widget.LinearLayoutManager
import com.testproject.R
import com.testproject.R.id.recycle_view
import com.testproject.TestApp
import com.testproject.common.mvp.MvpActivity
import com.testproject.model.Worlds
import com.testproject.ui.adapter.WorldsAdapter
import com.testproject.utils.EXTRA_WORLDS_DATA
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar.*
import timber.log.Timber
import javax.inject.Inject
import kotlin.properties.Delegates


class MainActivity : MvpActivity<MainActivityView, MainActivityPresenter>(), MainActivityView {

    @Inject
    lateinit var mainPresenter: MainPresenterImpl

    private var adapter: WorldsAdapter by Delegates.notNull()

    private lateinit var android_id: String
    private var data: ArrayList<Worlds>? = null


    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        data?.let {
            outState?.putParcelableArrayList(EXTRA_WORLDS_DATA, it)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        TestApp[this].component.inject(this)
        super.onCreate(savedInstanceState)
        android_id = Secure.getString(baseContext.getContentResolver(), Secure.ANDROID_ID)
        toolbar.title = getString(R.string.worlds_text)

        adapter = WorldsAdapter()
        recycle_view.adapter = adapter
        recycle_view.layoutManager = LinearLayoutManager(this)

        when {
            savedInstanceState != null -> {
                data = savedInstanceState.getParcelableArrayList(EXTRA_WORLDS_DATA)
                showWorldsList(data)
            }
            else ->
                if (TestApp.hasNetwork()) {
                    presenter?.provideWorlds(android_id)
                }
        }
    }

    override val layout: Int
        get() = R.layout.activity_main

    override fun createPresenter(): MainActivityPresenter {
        return mainPresenter
    }


    override fun showWorldsList(worlds: ArrayList<Worlds>?) {
        Timber.v(worlds.toString())
        data = ArrayList(worlds)
        worlds?.let {
            adapter.data = it
        }
    }

    override fun showErrorMessage(errorMessage: String) {
        showDialogErrorMessage(errorMessage);
    }

    override fun removeWait() {
        removeProgressWait()
    }

    override fun showWait() {
        showProgressWait()
    }
}
