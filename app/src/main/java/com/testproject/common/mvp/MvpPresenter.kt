package com.testproject.common.mvp

/**
@author Viktor Vaidner v.vaidner@theappsolutions.com
@copyright (c) 2018 TheAppSolutions. (https://theappsolutions.com)
@project testproject
 */
interface MvpPresenter<V : MvpView> {
    fun isViewAttached(): Boolean
    fun attachView(view: V)
    fun detachView()
}